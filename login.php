<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
    <link rel="stylesheet" href="dangki.css"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <title>Document</title>
</head>

<?php
$gtinh = array("0" => "Nam", "1" => "Nữ");
$khoa = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
$nameErr =$genderErr = $khoaErr = $dateErr =  "";
$name = $gender =$khoaA = $date = "";
const required = "is required";
//xác thực form bằng php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["fullname"])) {
        $nameErr = "Name " . required;
    }else {
        $name = test_input($_POST["fullname"]);
    }

    if (empty($_POST["gtinh"])) {
        $genderErr = "Gender " . required;
    }else {
        $gender = test_input($_POST["gtinh"]);
    }

    if (empty($_POST["khoa"])) {
        $khoaErr = "faculty " . required;
    }else {
        $khoaA = test_input($_POST["khoa"]);
    }

    if (empty($_POST["birthday"])) {
        $dateErr = "Date " . required;
    }else {
        $date = test_input($_POST["birthday"]);
    }
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function validateBirthday($birthday){
    $birthdays  = explode('/', $birthday);
    if (count($birthdays) == 3) {
      return checkdate($birthdays[1], $birthdays[0], $birthdays[2]);
    }
    return false;
}

echo '
    <form method="post" action="';echo htmlspecialchars($_SERVER["PHP_SELF"]); echo'">
    <div>'; 
        if (!empty($nameErr)) {
            echo '<span>';
            echo $nameErr;
            echo'</span>';
            echo '<br></br>';
        }
        if (!empty($genderErr)) {
            echo '<span>';
            echo $genderErr;
            echo'</span>';
            echo '<br></br>';
        }
        
        if (!empty($khoaErr)) {
            echo '<span>';
            echo $khoaErr;
            echo'</span>';
            echo '<br></br>';
        }

        if (!empty($dateErr)) {
            echo '<span>';
            echo $dateErr;
            echo'</span>';
            echo '<br></br>';
        }
        echo'
    </div class="div">
        <div class="div">
            <label class="label-left">Họ và tên<span>*</span></label>
            <input type="text" id="fullname" name="fullname"/>
        </div>

        <div class="div">
            <label class="label-left">Giới tính<span>*</span></label>';
            for ($i = 0; $i < count($gtinh); $i++) {
                echo "\t<input type=\"radio\" class=\"radio\" name=\"gtinh\" value=\"gtinh\">
                    <label for=\"{$i}\" class=\"gtinh\">{$gtinh[$i]}</label>\n";
            };
        echo '
        </div>

        <div class="div">
            <label class="label-left">Phân khoa<span>*</span></label>
            <select name="khoa" id="khoa" class="khoa">
                <option></option>';
                foreach ($khoa as $key => $value) {
                    echo "\t<option value=\"{$key}\">{$value}</option>\n";
                };
            echo '
            </select>
        </div>
        <div class="date">
            <label class="label-left">Ngày sinh<span>*</span></label>   
            <div class="input-group">
                <input class="form__input" id="birthday" name="birthday" placeholder="dd/mm/yyyy" type="text"/>
            </div>
        </div>
        

        <div class="div">
            <label class="label-left">Địa chỉ<span>*</span></label>
            <input type="text" class="address" id="address"/>
        </div>
        
        <button >Đăng nhập</button>
        
    </form>
';
?>
    <script>
            $(document).ready(function(){
                var date_input=$('input[name="birthday"]');
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                    format: 'dd/mm/yyyy',
                    container: container,
                    todayHighlight: true,
                    autoclose: true,
                })
            })
    </script>
    
</html>